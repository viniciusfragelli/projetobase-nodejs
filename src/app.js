const express = require('express');
const app = express();
const router = express.Router();
const pool = require('../Banco/pool-mysql');
const bodyParser = require('body-parser')
const connectionMiddleware = require('../Banco/connection-middleware');

//parsers
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(express.static('public'));

//Conexões
app.use(connectionMiddleware(pool));

//Rotas
app.use('/', require('./routes/api/TesteRoute'));

app.use(function(req, res, next){
    var json = {};
    json.status = "NOK";
    json.code = 0;
    json.msg = "Not found"
    res.status(404).send(json);
});

//Handler Error
app.use((errsystem,req,res,next) => {
    if(errsystem){
        if(req.isTransaction){
            req.connection.query("ROLLBACK",[],function (err, rs) {
                req.isTransaction = false;
                console.log("rollback")
                console.log(errsystem);
                var json = {};
                json.status = "NOK";
                json.code = 0;
                json.msg = "Não foi possível completar sua solicitação!"
                res.status(200).send(json);
            });
        }else{
            console.log("onError")
            console.log(errsystem)
            var json = {};
            json.status = "NOK";
            json.code = 0;
            json.msg = "Não foi possível completar sua solicitação!"
            res.status(200).send(json);
        }
    }else{
        next();
    }
});

module.exports = app;