exports.teste = (req, res, next) => {
    res.status(200).send("Requisição recebida com sucesso!"+req.body.teste);
};

exports.testebody = (req, res, next) => {
    res.status(200).send("recebi o body:"+req.body.teste);
};

exports.testequery = (req, res, next) => {
    var json = {}
    if(req.body.iduser == undefined)json.msg = "Entre com o usuário!";

    if(json.msg != undefined){
        res.status(200).send(json);
        return;
    }
    req.connection.query("SELECT * FROM usuarios WHERE iduser = ?",[req.body.iduser],function (err, rs) {
        if(err)return next(err);
        json.listaUsuarios = rs;
        res.status(200).send(json);
    });
};

exports.testequerysync = async (req, res, next) => {
    var json = {}
    if (req.body.iduser == undefined) json.msg = "Entre com o usuário!";

    try {
        var rs = await req.connectionAsync.query("SELECT * FROM usuarios WHERE iduser = ?", [req.body.iduser]);
        json.listaUsuarios = rs;
        res.status(200).send(json);
    }catch (e) {
        return next(e);
    }
};
