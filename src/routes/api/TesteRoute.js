const router = require('express').Router();

const controller = require('../../controllers/TesteController');

router.post('/testepost',controller.teste);
router.get('/testeget',controller.teste);
router.get('/testegetbodyuse',controller.testebody);
router.post('/testepostbodyuse',controller.testebody);
router.post('/testequery',controller.testequery);

module.exports = router;